﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerHealth : MonoBehaviour {

	public float fullHealth;
//	public restartGame theGameManager;

	float currentHealth;

	public AudioClip playerDamaged;
	AudioSource playerAS;

	//HUD references
	public Image healthSlider;
	public Image damageIndicator;
	public Text rubyCount;
	public CanvasGroup endGameCanvas;
	public Text EndGameText;

	Color flashColour = new Color (255f, 255f, 255f, 0.5f);
	float indicatorSpeed = 5f;

	//Player death
	playerControllerScript controlMovement;
	bool isDead;
	bool damaged;
	public GameObject playerDeathFX;


	//ruby collection
	int collectedRubies = 0;

    public GameObject healthSprite0, healthSprite1, healthSprite2, healthSprite3, healthSprite4;


    // Use this for initialization
    void Start () {
		currentHealth = fullHealth;
		healthSlider.fillAmount = 0f;
		controlMovement = GetComponent<playerControllerScript> ();
		playerAS = GetComponent<AudioSource> ();
		rubyCount.text = collectedRubies.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		//are we damaged?
		if (damaged) {
			damageIndicator.color = flashColour;
		} else {
			damageIndicator.color = Color.Lerp(damageIndicator.color, Color.clear, indicatorSpeed*Time.deltaTime);
		}
		damaged = false;
	
	}

	public void addDamage(float damage){
		if (damage <= 0)
			return;
		currentHealth -= damage;
		healthSlider.fillAmount = 1 - currentHealth/fullHealth;
		playerAS.clip = playerDamaged;
		playerAS.PlayOneShot (playerDamaged);

		damaged = true;
		if (currentHealth <= 0)
			makeDead ();
	}

	public void addHealth(float health){
		currentHealth += health;
		if (currentHealth > fullHealth)
			currentHealth = fullHealth;
		healthSlider.fillAmount = 1 - currentHealth/fullHealth;
	}

	public void makeDead(){
		//kill the player - death particles - destroy character - sound
		isDead = true;
		Instantiate (playerDeathFX, transform.position, transform.rotation);
		damageIndicator.color = flashColour;
		EndGameText.text = "You Died!";
		winGame();
		Destroy (gameObject);
	}

	public void addRuby(){
		collectedRubies +=1;
		rubyCount.text = collectedRubies.ToString();
		if(collectedRubies>11){
			EndGameText.text = "You Win!";
			GetComponent<playerControllerScript>().toggleCanMoveFalse();
			winGame();
		}
	}

	public void winGame(){
		endGameCanvas.alpha = 1f;
		endGameCanvas.interactable = true;
	}

    void ManageHeartUI()
    // This function is for displaying the amount of health the player has.
    //To achieve this I made the player health five different sprites that can turn on and off (setting as active)
    //depending on the players health will show a different sprite.
    {
        switch (currentHealth)
        {
            case 0:
                healthSprite4.SetActive(true);
                healthSprite0.SetActive(false);
                healthSprite1.SetActive(false);
                healthSprite2.SetActive(false);
                healthSprite3.SetActive(false);
                break;
            case 1:
                healthSprite3.SetActive(true);
                healthSprite0.SetActive(false);
                healthSprite1.SetActive(false);
                healthSprite2.SetActive(false);
                healthSprite4.SetActive(false);
                break;
            case 2:
                healthSprite2.SetActive(true);
                healthSprite0.SetActive(false);
                healthSprite1.SetActive(false);
                healthSprite3.SetActive(false);
                healthSprite4.SetActive(false);
                break;
            case 3:
                healthSprite1.SetActive(true);
                healthSprite0.SetActive(false);
                healthSprite2.SetActive(false);
                healthSprite3.SetActive(false);
                healthSprite4.SetActive(false);
                break;
            case 4:
                healthSprite0.SetActive(true);
                healthSprite1.SetActive(false);
                healthSprite2.SetActive(false);
                healthSprite3.SetActive(false);
                healthSprite4.SetActive(false);
                break;
        }
    }
}
